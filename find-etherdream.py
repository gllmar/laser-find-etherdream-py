import socket
import struct
import sys

def find_etherdream_devices(timeout=10):
    # EtherDream devices broadcast on UDP port 7654
    broadcast_port = 7654

    # Create a UDP socket
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
    sock.settimeout(timeout)

    try:
        # Bind the socket to the broadcast port
        sock.bind(('', broadcast_port))
    except Exception as e:
        print(f"Failed to bind to port {broadcast_port}: {e}")
        sys.exit(1)

    print(f"Listening for EtherDream devices for {timeout} seconds...")

    try:
        while True:
            data, addr = sock.recvfrom(1024)
            print(f"Found EtherDream device at {addr[0]}")
    except socket.timeout:
        print("Search complete.")

if __name__ == "__main__":
    find_etherdream_devices()
